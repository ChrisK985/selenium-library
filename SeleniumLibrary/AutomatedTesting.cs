﻿using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System.IO;
using OpenQA.Selenium.Interactions;

namespace SeleniumLibrary
{
    public class AutomatedTesting
    {
        public IWebDriver webd = new FirefoxDriver();
        //public OpenQA.Selenium.Chrome.ChromeDriver webd = new OpenQA.Selenium.Chrome.ChromeDriver(HttpContext.Current.Server.MapPath("/bin/"));
        
        public string log = "";

        
        public string checkElement(string path, string textToCheckFor = "", string attribute = "")
        {
            try
            {
                
                string text = (attribute != "" ? webd.FindElement(By.XPath(path)).GetAttribute(attribute) : webd.FindElement(By.XPath(path)).Text);
                if (textToCheckFor != "") return (text == textToCheckFor ? "SUCCESS" : string.Format("FAILURE - expected \"{0}\")", textToCheckFor));
                return (text != "" ? "SUCCESS" : "FAILURE - Element Empty");
            } catch (Exception ex)
            {
                appendLog(path + " - " + ex.Message);
                return "FAILURE - Check Log for Error";
            }
            
        }

        public void fillAllFormInputs(string path, List<string> inputNamesToSkip = null, int delay = 0)
        {
            Random rand = new Random();
            inputNamesToSkip = inputNamesToSkip ?? new List<string>();
            var inputs = webd.FindElement(By.XPath(path)).FindElements(By.TagName("input")).Where(i => !inputNamesToSkip.Contains(i.GetAttribute("name")));
            inputs.Where(i => i.GetAttribute("type") == "text" && i.Displayed).ToList().ForEach(i => i.SendKeys(i.GetAttribute("name")));
            inputs.Where(i => i.GetAttribute("type") == "email" && i.Displayed).ToList().ForEach(i => i.SendKeys("useremail" + rand.Next(1000) + "@gmail.com"));
            inputs.Where(i => i.GetAttribute("type") == "tel" && i.Displayed).ToList().ForEach(i => i.SendKeys(string.Format("{0}-{0}-{1}", rand.Next(100,999), rand.Next(1000,9999))));
            inputs.Where(i => i.GetAttribute("type") == "radio" || i.GetAttribute("type") == "checkbox" && i.Displayed).ToList().ForEach(i => i.Click());

            inputs = webd.FindElement(By.XPath(path)).FindElements(By.TagName("textarea")).Where(i => !inputNamesToSkip.Contains(i.GetAttribute("name")) && i.Displayed);
            inputs.ToList().ForEach(i => i.SendKeys(i.GetAttribute("name") + " - " + DateTime.Now.ToString("M/d/yy hh:mm")));

            inputs = webd.FindElement(By.XPath(path)).FindElements(By.TagName("select")).Where(i => !inputNamesToSkip.Contains(i.GetAttribute("name")) && i.Displayed);
            inputs.ToList().ForEach(i =>
            {
                SelectElement s = new SelectElement(i);
                if (s != null) s.SelectByIndex(s.Options.Count - 1);
            });

            System.Threading.Thread.Sleep(delay);
        }

        public void fillFormBySteps(List<ActionStep> steps)
        {
            int i = 1;
            Actions builder = new Actions(webd);


            foreach (ActionStep step in steps)
            {
                try
                {
                    
                    switch (step.action)
                    {
                        //path: XPath to exact object
                        case action.Click:
                            webd.FindElement(By.XPath(step.path)).Click(); break;
                        case action.Fill:
                            //value: text to fill object with
                            webd.FindElement(By.XPath(step.path)).SendKeys(step.value); break;
                        case action.DropDownSelect:
                            //value: look for specific dropdown option value.
                            SelectElement s = new SelectElement(webd.FindElement(By.XPath(step.path)));
                            if (!string.IsNullOrEmpty(step.value)) { s.SelectByValue(step.value); }
                            else { s.SelectByIndex(s.Options.Count - 1); }
                            break;
                       // case action.TakeScreenshot:
                            //takeScreenshot(step.path); break;
                        case action.LogElement:
                            //value: Append to start of log line
                            appendLog(step.value + webd.FindElement(By.XPath(step.path)).GetAttribute("innerHTML")); break;
                        case action.CheckElement:
                            appendLog(string.Format("{0}: {1} ({2})", step.name, webd.FindElement(By.XPath(step.path)).GetAttribute("innerText"), checkElement(step.path, step.value))); break;
                    }
                    System.Threading.Thread.Sleep(step.delay);
                }
                catch (Exception ex)
                {
                    appendLog(string.Format("Error on step {2}: {0} <br> {1}",JsonConvert.SerializeObject(step), ex.Message, i));
                }
                i++;
            }
        }

        /// <summary>
        /// Saves screenshot to root/screenshots
        /// </summary>
        /// <param name="elementToScrollTo">Uses site's jQuery to scroll to #element</param>
        /*
        public void takeScreenshot(string elementToScrollTo = null)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            if (!string.IsNullOrEmpty(elementToScrollTo))
            {
                IWebElement element = webd.FindElement(By.XPath(elementToScrollTo));
                ((IJavaScriptExecutor)webd).ExecuteScript("arguments[0].scrollIntoView(true);", element);
            }
            System.Threading.Thread.Sleep(500);

            Screenshot ss = ((ITakesScreenshot)webd).GetScreenshot();
            ss.SaveAsFile(string.Format("{0}{1}_{2}_{3}.jpg", HttpContext.Current.Server.MapPath("/screenshots/"), DateTime.Now.ToShortDateString().Replace("/", "-"), rgx.Replace(webd.Url, ""), rgx.Replace(elementToScrollTo, "")), ScreenshotImageFormat.Jpeg);
        }*/

        public void appendLog(string message)
        {
            log += string.Format("{0}: {1} <br>\r\n", DateTime.Now.ToString("M/d/yy hh:mmtt"), message);
        }

        ~AutomatedTesting()
        {
            webd.Quit();
            System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath("/logs/"), log);
        }

    }

    public enum action
    {
        Click,
        Fill,
        DropDownSelect,
        TakeScreenshot,
        LogElement,
        CheckElement
    }

    public class ActionStep
    {
        public string name { get; set; }
        public string path { get; set; }
        public string value { get; set; }
        public int delay { get; set; }

        

        public SeleniumLibrary.action action { get; set; }

    }
}
